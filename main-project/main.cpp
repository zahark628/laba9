#include <iostream>
#include <iomanip>

using namespace std;

#include "exam.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Ëàáîðàòîðíàÿ ðàáîòà ¹8. GIT\n";
    cout << "Âàðèàíò ¹8. Èòîãè ñåññèè\n";
    cout << "Àâòîð: Äìèòðèé Ïàíèí\n";
    cout << "Ãðóïïà: 14\n\n";
    
    exam_info* exams[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", exams, size);
        cout << "***** Èòîãè ñåññèè *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** âûâîä ñòóäåíòà **********/
            cout << "Ñòóäåíò........: ";
            // âûâîä ôàìèëèè
            cout << exams[i]->student.last_name << " ";
            // âûâîä ïåðâîé áóêâû èìåíè
            cout << exams[i]->student.first_name[0] << ". ";
            // âûâîä ïåðâîé áóêâû îò÷åñòâà
            cout << exams[i]->student.middle_name[0] << ".";
            cout << '\n';
            
            // âûâîä äèñöèïëèíû
            cout << '"' << exams[i]->discipline << '"';
            cout << '\n';

            // âûâîä îöåíêè
            cout << "Îöåíêà.........: ";
            cout << exams[i]->mark;
            cout << '\n';
            /********** âûâîä äàòû ñäà÷è **********/
            // âûâîä ãîäà
            cout << "Äàòà ñäà÷è.....: ";
            cout << setw(4) << setfill('0') << exams[i]->exam_day.year << '-';
            // âûâîä ìåñÿöà
            cout << setw(2) << setfill('0') << exams[i]->exam_day.month << '-';
            // âûâîä ÷èñëà
            cout << setw(2) << setfill('0') << exams[i]->exam_day.day;
            cout << '\n';
           
            cout << '\n';
        }

        bool (*check_function)(exam_info*) = NULL;
        cout << "\nÂûáåðèòå ñïîñîá ôèëüòðàöèè äàííûõ:\n";
        cout << "1) Ñòóäåíòû è èõ îöåíêè ïî äèñöèïëèíå ""Èñòîðèÿ Áåëàðóñè""\n";
        cout << "2) Ñòóäåíòû, ïîëó÷èâøèå îòìåòêó 7 èëè âûøå çà äèñöèïëèíó\n";
        cout << "3) Äëèòåëüíîñòü ñåññèè\n";
        cout << "\nÂâåäèòå íîìåð âûáðàííîãî ïóíêòà: ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_discipline; 
            cout << "***** Ñòóäåíòû è èõ îöåíêè ïî äèñöèïëèíå ""Èñòîðèÿ Áåëàðóñè"" *****\n\n";
            break;
        case 2:
            check_function = check_mark_value;
            cout << "***** Ñòóäåíòû, ïîëó÷èâøèå îòìåòêó 7 èëè âûøå çà äèñöèïëèíó *****\n\n";
            break;
        case 3:
            cout << "Ñåññèÿ äëèëàñü " << process(exams, size) << " äíÿ(åé)\n\n";
            break;
        default:
            throw "Íåêîððåêòíûé íîìåð ïóíêòà";
        }

        if (check_function) {
            int new_size;
            exam_info** filtered = filter(exams, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
               
                cout << "Ñòóäåíò........: ";
              
                cout << filtered[i]->student.last_name << " ";
                
                cout << filtered[i]->student.first_name[0] << ". ";
               
                cout << filtered[i]->student.middle_name[0] << ".";
                cout << '\n';

               
                cout << '"' << filtered[i]->discipline << '"';
                cout << '\n';

                
                cout << "Îöåíêà.........: ";
                cout << filtered[i]->mark;
                cout << '\n';
                cout << '\n';

            }
            delete[] filtered;
        }
        
        for (int i = 0; i < size; i++)
        {
            delete exams[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
