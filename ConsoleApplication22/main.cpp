﻿// ConsoleApplication22.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <iomanip>

using namespace std;

#include "exam.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Laboratory work #9. GIT\n";
    cout << "Variant #8. Session results\n";
    cout << "Author: Kuznetsov Zakhar\n";
    cout << "Group - 14\n";

    exam_info* exams[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", exams, size);
        cout << "***** Информация о экзаменах *****\n\n";
        for (int i = 0; i < size; i++)
        {
            
            cout << "ФИО........: ";
            
            cout << exams[i]->student.last_name << " ";
            
            cout << exams[i]->student.first_name[0] << ". ";
            
            cout << exams[i]->student.middle_name[0] << ".";
            cout << '\n';

            
            cout << '"' << exams[i]->discipline << '"';
            cout << '\n';

            
            cout << "Оценка.........: ";
            cout << exams[i]->mark;
            cout << '\n';
            
            
            cout << "Дата сдачи....: ";
            cout << setw(4) << setfill('0') << exams[i]->exam_day.year << '-';
           
            cout << setw(2) << setfill('0') << exams[i]->exam_day.month << '-';
            
            cout << setw(2) << setfill('0') << exams[i]->exam_day.day;
            cout << '\n';

            cout << '\n';
        }

        bool (*check_function)(exam_info*) = NULL;
        cout << "\nОценки:\n";
        cout << "1) Высший балл по предмету ""История Беларуси""\n";
        cout << "2) Вывод учеников, с баллом выше 7\n";
        cout << "3) Вывести длительность сессии в днях (разница между минимальной и максимальной датой экзамена)\n";
        cout << "\n Введите номер:\n ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_discipline; 
            cout << "***** Высший балл выставлен по предмету ""История Беларуси"" *****\n\n";
            break;
        case 2:
            read("data.txt", exams, size);
            check_function = check_mark_value; 
            cout << "***** Ученики, с баллом выше 7*****\n\n";
            break;
        case 3:
            cout << "Длительность сессии " << process(exams, size) << " дня(-ей)\n\n";
            break;
        default:
            throw "Введенно ошибочное значение!";
        }

        if (check_function) {
            int new_size;
            exam_info** filtered = filter(exams, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
               
                cout << "Ñòóäåíò........: ";
                
                cout << filtered[i]->student.last_name << " ";
                
                cout << filtered[i]->student.first_name[0] << ". ";
                
                cout << filtered[i]->student.middle_name[0] << ".";
                cout << '\n';

                
                cout << '"' << filtered[i]->discipline << '"';
                cout << '\n';

               
                cout << "Îöåíêà.........: ";
                cout << filtered[i]->mark;
                cout << '\n';
                cout << '\n';

            }
            delete[] filtered;
        }

        for (int i = 0; i < size; i++)
        {
            delete exams[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
